//todo: use strict;

var express = require('express'),
	app = express(),
	photos = require('./routes/photos'),
	upload = require('./upload');


app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.get('/', function (req, res) {
  res.send('Hello Nixplay!');
});

app.get('/photos/list', photos.showAll);


app.get('/photos/list/:id/details', photos.showDetails);


app.post('/photo/upload', upload.single('data'), photos.upload);


app.use('/uploads', express.static(__dirname + '/uploads'));


app.use(function(err, req, res, next) {

  res.status(500).send(err.message);
});

var server = app.listen(3000,  function () {
  console.log('Nixplay app listening on port ' + server.address().port );
});