var fs = require('fs'),
    sizeOf = require('image-size');
	path = require('path');

var PATH_TO_UPLOAD = './uploads';


var getFilesData = function (req, res, next, callback) {

	var filesData = [];

    fs.readdir(PATH_TO_UPLOAD, function(err, files) {

        if (err) {
            next(new Error(err));
        }

        files.sort(function(a, b) {

            return a < b ? -1 : 1;

        }).forEach(function(file, index) {

        	var ext = path.extname(file);

        	//crude way to limit to images
        	if(ext === '.png' || ext === '.jpg' || ext === '.svg' || ext === '.gif') {

	            var filePath = path.format({ dir: PATH_TO_UPLOAD, base: file }),

                data = {
                    id: index,
                    name: file,
                    path: req.protocol + '://' + req.get('host') + '/' + path.normalize(filePath)
                };


	            filesData.push(data);

        	}


        });

        callback(filesData);

    });
};

var photos = {


    showAll: function(req, res, next) {

    	getFilesData(req, res, next, function (filesData) {

    		res.send(filesData);
    	})

    },
    showDetails: function(req, res, next) {


        var id = req.params.id,
        	filePath;

        	//this is in case this endpoint is accessed directly so there is no way of identifying id of image
			getFilesData(req, res, next, function(filesData) {

	            var file = filesData.filter(function(file) {
	                return file.id == id;
	            })[0];

	            if(!file) {
	            	return next(new Error('file does not exist'));
	            }

	            filePath = path.format({ dir: PATH_TO_UPLOAD, base: file.name })

	        	sizeOf(filePath, function (err, dimensions) {

	        		if(err) {
	        			return next(new Error(err));
	        		}

	          	    res.send({
	          	    	id: id,
	          	    	name: file.name,
	          	    	width: dimensions.width, 
	          	    	height: dimensions.height
	          	    });

	        	});

			}, next);

    },
    upload: function (req, res, next) {

		//res.sendStatus(200);

    	getFilesData(req, res, next, function (filesData) {

    		res.send(filesData);
    	})

    }
};


module.exports = photos;

