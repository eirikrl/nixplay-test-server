var multer  = require('multer'),
	path = require('path');

var PATH_TO_UPLOAD = './uploads';	


var storage = multer.diskStorage({

  destination: function (req, file, cb) {
    cb(null, PATH_TO_UPLOAD);
  },
  filename: function (req, file, cb) {

  	var extension = path.extname(file.originalname),
		name = path.basename(file.originalname, extension);

    cb(null, name + '-' + Date.now() + extension);
  }
});

var upload = multer({ 
	
	storage: storage,
	fileFilter: function (req, file, cb) {

	  var mimetype = file.mimetype;

	  if( mimetype !== 'image/png' && mimetype !== 'image/jpeg' && mimetype !== 'image/gif' && mimetype !== 'image/svg+xml') {
	  	cb(new Error('Not valid type.  Supports png, jpeg, gif, and svg only'));
	  } else {
	    cb(null, true)
	  }
	}
});

var photoUpload = {

	single: function (data) {
		return upload.single(data);
	}
}

module.exports = photoUpload;
